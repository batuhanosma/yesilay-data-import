﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class PublisherRepository
    {
        private readonly YesilayTbaEntities _context;
        public PublisherRepository()
        {
            _context = new YesilayTbaEntities();
        }
        public PublisherRepository(YesilayTbaEntities context)
        {
            _context = context;
        }
        public Publishers GetByName(string name)
        {
            var publisher = _context.Publishers.FirstOrDefault(x => x.Name == name);
            return publisher;
        }

        public void Create(Publishers publisher)
        {
            _context.Publishers.Add(publisher);
            _context.SaveChanges();
        }
    }
}
