﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class LanguageRepository
    {
        private readonly YesilayTbaEntities _context;
        public LanguageRepository()
        {
            _context = new YesilayTbaEntities();
        }
        public LanguageRepository(YesilayTbaEntities context)
        {
            _context = context;
        }
        public ImprintLanguages GetByName(string name)
        {
           var lang = _context.ImprintLanguages.FirstOrDefault(x => x.LanguageName == name);
            return lang;
        }

        public void Create (ImprintLanguages language)
        {
            _context.ImprintLanguages.Add(language);
            _context.SaveChanges();
        }
    }
}
