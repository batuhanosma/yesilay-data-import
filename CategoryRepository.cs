﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class CategoryRepository
    {
        private readonly YesilayTbaEntities _context;
        public CategoryRepository()
        {
            _context = new YesilayTbaEntities();
        }
        public CategoryRepository(YesilayTbaEntities context)
        {
            _context = context;
        }

        public ImprintCategories GetByName(string name)
        {
            var category = _context.ImprintCategories.FirstOrDefault(x => x.Name == name);
            return category;
        }

        public void Create(ImprintCategories category)
        {
            _context.ImprintCategories.Add(category);
            _context.SaveChanges();
        }
    }
}
