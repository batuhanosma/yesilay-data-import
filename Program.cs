﻿using ClosedXML.Excel;
using Data;
using System;
using System.Collections.Generic;
using System.Transactions;
//using System;

namespace YesilayDataImport
{
    class Program
    {
        const string File_Path = "C:\\Repos\\PixelPlus\\YesilayImportSon\\YesilayDataImport\\aktif.xlsx";
        static void Main(string[] args)
        {

            ReadExcel();

        }

        static void ReadExcel()
        {
            var dbContext = new YesilayTbaEntities();
            var languageRepository = new LanguageRepository(dbContext);
            var authorRepository = new AuthorRepository(dbContext);
            var publisherRepository = new PublisherRepository(dbContext);
            var categoryRepository = new CategoryRepository(dbContext);
            var tagRepository = new TagRepository(dbContext);

            int i = 2;
            var workbook = new XLWorkbook(File_Path);
            var ws1 = workbook.Worksheet(1);

            var recomendedImprintList = new List<RecomendedImprints>();
            var languages = new List<ImprintLanguages>();

            do
            {
                var row = ws1.Row(i);

                if (row.IsEmpty() || i == 100)
                {
                    break;
                }

                var imprintTypeStr = row.Cell(3)?.Value?.ToString();
                ImprintTypes imprintTypeValue;

                if (Enum.TryParse(imprintTypeStr, true, out imprintTypeValue))
                {
                    if (Enum.IsDefined(typeof(ImprintTypes), imprintTypeValue)){
                        //başarılı
                    }
                }
                else
                {
                    imprintTypeValue = ImprintTypes.Other;

                }

                var language = row.Cell(5)?.Value?.ToString();
                var languageObj = languageRepository.GetByName(language);
                if(languageObj == null)
                {
                    languageRepository.Create(new ImprintLanguages() { LanguageName = language });
                    languageObj = languageRepository.GetByName(language);
                }

                var authorStr = row.Cell(6)?.Value?.ToString();
                Authors author = authorRepository.GetByName(authorStr);
                if(author == null)
                {
                    authorRepository.Create(new Authors() { FullName = authorStr});
                    author = authorRepository.GetByName(authorStr);
                }

                var publisherStr = row.Cell(7)?.Value?.ToString();
                Publishers publisher = publisherRepository.GetByName(publisherStr);
                if(publisher == null)
                {
                    publisherRepository.Create(new Publishers() { Name = publisherStr});
                    publisher = publisherRepository.GetByName(publisherStr);
                }

                var publicationYearStr = row.Cell(8)?.Value?.ToString();
                int? publicationYearInt = null;
                if(!string.IsNullOrEmpty(publicationYearStr))
                {
                    publicationYearInt = Convert.ToInt32(publicationYearStr);
                }


                var pageCountStr = row.Cell(10)?.Value?.ToString();
                int? pageCountInt = null;
                if(!string.IsNullOrEmpty(pageCountStr))
                {
                    pageCountInt = Convert.ToInt32(pageCountStr);
                }


                var isInLibraryStr = row.Cell(13)?.Value?.ToString();
                bool? inLibrary = null;
                if(isInLibraryStr?.ToLower() == "evet")
                {
                    inLibrary = true;
                }

                var hasPdfStr = row.Cell(14)?.Value?.ToString();
                bool? hasPdf = false;
                if(hasPdfStr?.ToLower() == "var")
                {
                    hasPdf = true;
                }

                var volNumberStr = row.Cell(18)?.Value?.ToString();
                int? volNumber = null;
                if(!string.IsNullOrEmpty(volNumberStr))
                {
                    volNumber = Convert.ToInt32(volNumberStr);
                }

                var issueNumberStr = row.Cell(19)?.Value?.ToString();
                int? issueNumber = null;
                if (!string.IsNullOrEmpty(issueNumberStr))
                {
                   issueNumber = Convert.ToInt32(issueNumberStr);
                }


                var publishYearStr = row.Cell(20)?.Value?.ToString();
                int? publishYear = null;
                if (!string.IsNullOrEmpty(publishYearStr))
                {
                    publishYear = Convert.ToInt32(publishYearStr);
                }


                var thesisYearStr = row.Cell(26)?.Value?.ToString();
                int? thesisYear = null;
                if(!string.IsNullOrEmpty(thesisYearStr))
                {
                    thesisYear = Convert.ToInt32(thesisYearStr);
                }

                var recomendedImprint = new RecomendedImprints()
                {
                    FullName = row.Cell(2)?.Value?.ToString(),
                    ImprintType = (int)imprintTypeValue,
                    ImprintLanguageId = languageObj.Id,
                    AuthorId = author.Id,
                    PublisherId = publisher.Id,
                    PublicationYear = publicationYearInt,
                    Location = row.Cell(9)?.Value?.ToString(),
                    PageCount = pageCountInt,
                    IsInLibrary = inLibrary,
                    HasPdf = hasPdf,
                    SourceLink = row.Cell(15)?.Value?.ToString(),
                    JournalName = row.Cell(16)?.Value?.ToString(),
                    PageRange = row.Cell(17)?.Value?.ToString(),
                    VolNumber = volNumber,
                    IssueNumber = issueNumber,
                    PublishYear = publishYear,
                    Summary = row.Cell(21)?.Value?.ToString(),
                    University = row.Cell(22)?.Value?.ToString(),
                    Faculity = row.Cell(23)?.Value?.ToString(),
                    Department = row.Cell(24)?.Value?.ToString(),
                    ThesisAdvisor = row.Cell(25)?.Value?.ToString(),
                    ThesisYear = thesisYear,
                    YokThesisNumber = row.Cell(27)?.Value?.ToString(),
                    IBSN = row.Cell(29)?.Value?.ToString(),
                    UnitName = row.Cell(35)?.Value?.ToString(),
                    FixtureNo = row.Cell(36)?.Value?.ToString(),
                    CatalogRecid = row.Cell(38)?.Value?.ToString(),
                    CatalogRelation = row.Cell(39)?.Value?.ToString(),
                    CreatedDate = DateTime.Now,
                    ImprintStatus = 2,
                    
                };

                var imprintCategories = new List<ImprintCategories>();
                var imprintTags = new List<ImprintTags>();

                //TODO: KATEGORİ, ETİKET
                var categoriesStr = row.Cell(11)?.Value?.ToString();
                var categoryList = categoriesStr.Split(',');
                for (int j = 0; j < categoryList.Length; j++)
                {
                    var category = categoryRepository.GetByName(categoryList[j]);
                    if(category == null)
                    {
                        var newCategoryToCreate = new ImprintCategories();
                        newCategoryToCreate.Name = categoryList[j];
                        categoryRepository.Create(newCategoryToCreate);

                        newCategoryToCreate = categoryRepository.GetByName(categoryList[j]);

                        var newCategory = new ImprintCategories()
                        {
                            Name = newCategoryToCreate.Name,
                            Id = newCategoryToCreate.Id
                        };

                        imprintCategories.Add(newCategory);

                    }
                    else
                    {
                        var newCategory = new ImprintCategories()
                        {
                            Id = category.Id,
                            Name = category.Name
                        };

                        imprintCategories.Add(newCategory);
                    }
                }

                var tagStr = row.Cell(12)?.Value?.ToString();
                var tagList = tagStr.Split(',');
                for (int j = 0; j < tagList.Length; j++)
                {
                    var tag = tagRepository.GetByName(tagList[j]);
                    if (tag == null)
                    {
                        var newTagToCreate = new ImprintTags();
                        newTagToCreate.Name = tagList[j];
                        tagRepository.Create(newTagToCreate);

                        newTagToCreate = tagRepository.GetByName(tagList[j]);

                        var newTag = new ImprintTags()
                        {
                            Id = newTagToCreate.Id,
                            Name = newTagToCreate.Name
                        };

                        imprintTags.Add(newTag);
                    }
                    else
                    {
                        var newTag = new ImprintTags()
                        {
                            Id = tag.Id,
                            Name = tag.Name
                        };
                        imprintTags.Add(newTag);
                    }
                }


                recomendedImprint.ImprintTags = imprintTags;
                recomendedImprint.ImprintCategories = imprintCategories;

                recomendedImprintList.Add(recomendedImprint);

                Console.WriteLine($"{i}. kayit eklendi");

                i++;
            } while (true);

            Console.WriteLine("Database e yükleme basladi************************************************************");
            BulkInsert(recomendedImprintList);

        }


        //https://stackoverflow.com/a/5942176/7293218
        static void BulkInsert(List<RecomendedImprints> recomendedImprints)
        {

            using (TransactionScope scope =
             new TransactionScope(TransactionScopeOption.Required,
                                   new System.TimeSpan(0, 15, 0)))
            {
                YesilayTbaEntities context = null;
                try
                {
                    context = new YesilayTbaEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;

                    int count = 0;
                    foreach (var entityToInsert in recomendedImprints)
                    {
                        ++count;
                        context = AddToContext(context, entityToInsert, count, 100, true);
                    }

                    context.SaveChanges();
                    Console.WriteLine("Yuz kayit db ye eklendi");
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }

                scope.Complete();
            }
        }



        private static YesilayTbaEntities AddToContext(YesilayTbaEntities context,
    RecomendedImprints entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<RecomendedImprints>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new YesilayTbaEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        enum ImprintTypes
        {
            Article,
            Book,
            Thesis,
            Other
        }
    }
}
