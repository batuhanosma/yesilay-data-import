﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class AuthorRepository
    {
        private readonly YesilayTbaEntities _context;
        public AuthorRepository()
        {
            _context = new YesilayTbaEntities();
        }
        public AuthorRepository(YesilayTbaEntities context)
        {
            _context = context;
        }

        public Authors GetByName(string name)
        {
            var author = _context.Authors.FirstOrDefault(x => x.FullName == name);
            return author;
        }

        public void Create(Authors author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
        }
    }
}
