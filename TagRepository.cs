﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YesilayDataImport
{
    public class TagRepository
    {
        private readonly YesilayTbaEntities _context;
        public TagRepository()
        {
            _context = new YesilayTbaEntities();
        }
        public TagRepository(YesilayTbaEntities context)
        {
            _context = context;
        }

        public ImprintTags GetByName(string name)
        {
            var tag = _context.ImprintTags.FirstOrDefault(x => x.Name == name);
            return tag;
        }

        public void Create(ImprintTags tag)
        {
            _context.ImprintTags.Add(tag);
            _context.SaveChanges();
        }
    }
}
